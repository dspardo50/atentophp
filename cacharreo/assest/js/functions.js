
/**
 * Panel Administrativo para extraccion de datos excel
 * Version: 1.0.1
 * Author: Jesus Moran
 * Author URI: 
 */

$(document).ready(function(){
    $('#btnnewadmin').on('click', function(e){
        e.preventDefault(); 
        $('#btnnewadmin').hide();
        const data = $('#frmNewAdmin').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=newadmin',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'REGISTRO_REALIZADO':
                            swal({
                                title: "Exito",
                                text: "El registro ha sido actualizado",
                                icon: "success",
                            });
                            $('#btnnewadmin').show();
                            $('#frmNewAdmin')[0].reset();
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion guardar admin

    $('#btnupdateprofile').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmProfile').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=updateprofile',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "El registro ha sido actualido de forma exitosa",
                                icon: "success",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion actualizar perfil

    $('#btnpaswd').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmPassword').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=updatepassword',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "El registro ha sido actualido de forma exitosa",
                                icon: "success",
                            });
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "No pudimos actualizar tu clave, hay un error en la clave actual, por favor verifica e intenta nuevamente",
                                icon: "warning",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion actualizar contraseña


    $('#btnUpdadmin').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmUpdateAdmin').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=update__admin',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "El registro ha sido actualido de forma exitosa",
                                icon: "success",
                            });
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "No pudimos actualizar tu clave, hay un error en la clave actual, por favor verifica e intenta nuevamente",
                                icon: "warning",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion editar admin


    $('#btnlogin').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmlogin').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=login',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'LOGIN':
                            window.location.href = 'index.php';
                        break; 
                        case 'ERROR':
                            window.location.href = 'login.php?error=errorcode';
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax

    });
    
    //correcciones
    
    $('#btn_csx').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmxcd').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/proyectos.php?op=_correcciones',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'OK':
                            swal({
                                title: "Exito",
                                text: "Se ha almacenado correctamente la información",
                                icon: "success",
                            });
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "Ha ocurrido un error almacenando la información",
                                icon: "danger",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });
    //fin correcciones

    $('#btn-registro_').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmpersonal').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=__save',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    switch(response.msg){
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "Se ha almacenado correctamente la información",
                                icon: "success",
                            });
                            $('#frmpersonal')[0].reset();
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Houston, hay un problema",
                                text: "Al parecer existe hubo un error al procesar la solicitud, verifica que el usuario que estas agregando no exista en la base de datos.",
                                icon: "success",
                            });
                            $('#frmpersonal')[0].reset();
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion guardar coordinador y tutores


    $('#btnRegistro').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmRegistro').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=registro',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    console.log(response)
                    switch(response.msg){
                        
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "Su cuenta ha sido creada de forma exitosa, revisa tu correo y verificalo para usar nuestros servicios",
                                icon: "success",
                            });
                            $('#frmRegistro')[0].reset();
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "No pudimos crear tu cuenta, el correo ya aparece en nuestros registros",
                                icon: "warning",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion registrar usuario prueba


    $('#btnlinea').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmlinea').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=__linea',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    console.log(response)
                    switch(response.msg){
                        
                        case 'SUCCESS':
                            swal({
                                title: "Exito",
                                text: "La información se ha almacenado correctamente",
                                icon: "success",
                            });
                            $('#frmlinea')[0].reset();
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "Hubo un error guardando la información",
                                icon: "warning",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion registrar lineas de investigacion

    
    $('#btnlinea__upd').on('click', function(e){
        e.preventDefault(); 
        const data = $('#frmlineaupd').serialize();
        $.ajax({
            type: 'post',
            url: 'controllers/manager.php?op=__updlinea',
            data: data,
            dataType: 'json',
            success:function(response){
                if(response.msg){
                    console.log(response)
                    switch(response.msg){
                        case 'SUCCESS':
                            
                        break; 
                        case 'ERROR':
                            swal({
                                title: "Error",
                                text: "Hubo un error guardando la información",
                                icon: "warning",
                            });
                        break; 
                    }//fin switch
                }//fin response.msg
            }//fin response
        });//fin funcion ajax
    });//fin funcion registrar lineas de investigacion
    
}); //Fin de document ready


  //borrar admin
function borraradmin(idmanager){
    swal({
        title: "¿Está seguro?",
        text: "¿Está seguro que desea eliminar el registro? Esta acción no se puede deshacer",
        icon: "warning",
        buttons: true,
        dangermode: true,
    }).then((willdelete)=> {
        if(willdelete){
        fncDeleteadmin(idmanager);    
          location.reload();
        } else {
        swal("La acción fue cancelada");    
        }
    });
}


async function fncDeleteadmin(idmanager) {
    try {
      let formData = new FormData();
      formData.append('idmanager', idmanager); 
  
      let response = await fetch("controllers/manager.php?op=delete__admin",{
        method: 'post',
        mode: 'cors',
        body: formData,
        cache: 'no-cache',
      });
      console.log(response.msg);
      if (response.msg) {
        switch(response.msg){
            
          case 'SUCCESS':
          break;
          case 'ERROR':
            console.log("Error eliminando")
          break;
        }
      }
    } catch (error) {
      console.log("Ocurrio un error:" + error);
    }
  }

  //fin de borrar admin
  function abrir(){

    var bs_modal = $('#modal');
    bs_modal.modal('show');
  }


    //borrar admin
function borrarlinea(id, linea){
    swal({
        title: "¿Está seguro?",
        text: "¿Está seguro que desea eliminar el registro? Esta acción no se puede deshacer",
        icon: "warning",
        buttons: true,
        dangermode: true,
    }).then((willdelete)=> {
        if(willdelete){
        fncDeleteLine(id, linea);    
          location.reload();
        } else {
        swal("La acción fue cancelada");    
        }
    });
}


async function fncDeleteLine(id, linea) {
    try {
      let formData = new FormData();
      formData.append('id', id); 
      formData.append('linea', linea); 
  
      let response = await fetch("controllers/manager.php?op=delete__linea",{
        method: 'post',
        mode: 'cors',
        body: formData,
        cache: 'no-cache',
      });
      console.log(response.msg);
      if (response.msg) {
        switch(response.msg){
            
          case 'SUCCESS':
            location.reload();
          break;
          case 'ERROR':
            console.log("Error eliminando")
          break;
        }
      }
    } catch (error) {
      console.log("Ocurrio un error:" + error);
    }
  }

  //fin de borrar admin

  //funcion para extraer todos los registros de los tutores
  async function getTutors() {
    

    try {
      let resp = await fetch("controllers/tutores.php?op=__listado");
      json = await resp.json();
      if (json.status) {
        let data = json.data;
        let tableBody = document.querySelector("#tutores"); // Obtener el cuerpo de la tabla
        data.forEach(item => {
          let newtr = document.createElement("tr");
          newtr.id = "row_" + item.id;
          newtr.innerHTML = `
            <td class="d-xl-table-cell">${item.p_nombre} ${item.p_apellido}</td>
            <td class="d-xl-table-cell">${item.identificacion}</td>
            <td class="d-xl-table-cell">${item.celular}</td>
            <td class="d-xl-table-cell">${item.email}</td>
            <td class="d-md-table-cell">
              <button onclick="asignarTutor(${item.idpersona})" class="btn btn-warning"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-check icontb"></i></button> 
              <a href="index.php?p=cambiar&id=${item.idpersona}" class="btn btn-danger"  data-bs-toggle="tooltip" data-bs-placement="top" title="Cambiar tutor"><i class="fa-solid fa-pen-to-square icontb"></i></a> 
            </td>
          `;
          tableBody.appendChild(newtr); // Agregar el registro al cuerpo de la tabla
        });
      }
    } catch (error) {
      console.log("Ocurrio un error" + error);
    }
  }
  
  // Verificar si existe el elemento de tabla antes de llamar a getTutors()
  if (document.querySelector("#tutores")) {
    getTutors();
  }
  


  //buscador de tutores
  if(document.querySelector('#frmBuscar')){
    let frmBuscar = document.querySelector('#frmBuscar');
    let inputBuscar = document.querySelector('#txtbuscador');
    inputBuscar.addEventListener("keyup", fnInpBuscar, true);


    frmBuscar.onsubmit = function(e){
        e.preventDefault();
        let busqueda = document.querySelector("#txtbuscador").value;
        if(busqueda == ""){
            document.querySelector('#tutores').innerHTML=""
            getTutors();
        } else {
            fnBuscarTutor();
        }

    }
    
}

async function fnBuscarTutor(busqueda){
    document.querySelector('#tutores').innerHTML="";

    try {

        let formData = new FormData();
        formData.append('buscar', busqueda);
        let resp = await fetch('controllers/tutores.php?op=__buscar', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });

        json = await resp.json();
        if (json.status){
            let data = json.data;
            data.forEach(item => {
                let newtr = document.createElement("tr");
                newtr.id = "row_"+item.id;
                newtr.innerHTML=`
                    <tr>
                        <td class="d-xl-table-cell">${item.p_nombre} ${item.p_apellido}</td>
                        <td class="d-xl-table-cell">${item.identificacion}</td>
                        <td class="d-xl-table-cell">${item.celular}</td>
                        <td class="d-xl-table-cell">${item.email}</td>
                        <td class="d-md-table-cell">
                            <button onclick="asignarTutor(${item.idpersona})" class="btn btn-warning"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-check icontb"></i></button> 
                            <a href="index.php?p=cambiar&id=${item.idpersona}" class="btn btn-danger"  data-bs-toggle="tooltip" data-bs-placement="top" title="Cambiar tutor"><i class="fa-solid fa-pen-to-square icontb"></i></a> 
                        </td>
                    </tr>`;
                    document.querySelector("#tutores").innerHTML="";
                    document.querySelector("#tutores").appendChild(newtr);
            });
        }

        
    } catch (error) {
        console.log("Error en la solicitud" +error)
        
    }

}

function fnInpBuscar(){
    let busqueda = document.querySelector("#txtbuscador").value;
        if(busqueda == ""){
            document.querySelector('#tutores').innerHTML=""
            getTutors();
        } else {
            fnBuscarTutor(busqueda);
        }
  }
  
  var bs_modal = $('#tutorModal');
  
  function asignarTutor(idpersona){
    // Obtén la URL actual
    var url = new URL(window.location.href);

    // Obtén los parámetros de la URL
    var params = new URLSearchParams(url.search);

    // Obtiene el valor de la variable "id"
    var id = params.get('id');

    
    bs_modal.modal('show');

    var idtutor = document.getElementById('idpersona');
    idtutor.value=idpersona;

    var idproyecto = document.getElementById('idproyecto');
    idproyecto.value =id;

  }

  if(document.querySelector('#guardar_tutor')){
    document.querySelector('#guardar_tutor').addEventListener('click', function(e) {
        console.log("estoy aqui");
            e.preventDefault();
            const idpersona = document.getElementById('idpersona').value;
            const rol_tutor = document.getElementById('rol_tutor').value;
            const fase = document.getElementById('fase_proyecto').value;
            const idproyecto = document.getElementById('idproyecto').value;
        
            swal({
                title: "Asignación de tutor",
                text: "¿Está seguro que desea asignar este tutor al proyecto?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((asignacion) => {
                if (asignacion) {
                    bs_modal.modal('hide');
                    asignaTutor(idpersona, idproyecto, rol_tutor, fase);     
                } else {
                    swal("La acción fue cancelada");    
                }
            });
        });
    
  }
  

  async function asignaTutor(idpersona, idproyecto, rol_tutor, fase){
    try {
        let formData = new FormData();
        formData.append("idtutor", idpersona);
        formData.append("idproyecto", idproyecto);
        formData.append("rol_tutor", rol_tutor);
        formData.append("fase", fase);

        let resp = await fetch("controllers/tutores.php?op=__asignar", {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            body: formData
        })
        json = await resp.json();
        if(json.mensaje=="OK"){
            swal({
                title: "Exito",
                text: "El tutor ha sido asignado de forma exitosa",
                icon: "success"
            });
        } if(json.mensaje=="EXISTE"){
            swal({
                title: "Error",
                text: "El tutor ya esta asignado a este proyecto",
                icon: "warning"
            });
        }

        
    } catch (error) {
        console.log("Ha ocurrido un error en la transaccion " + error);
    }
  }


  async function asignarAlumno(idpersona, idproyecto){
    try {
        let formData = new FormData();
        formData.append("idpersona", idpersona);
        formData.append("idproyecto", idproyecto);

        let resp = await fetch("controllers/alumnos.php?op=__asignar", {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            body: formData
        })
        json = await resp.json();
        if(json.mensaje=="OK"){
            swal({
                title: "Exito",
                text: "El alumno ha sido registrado al proyecto de forma exitosa",
                icon: "success"
            });
        } if(json.mensaje=="EXISTE"){
            swal({
                title: "Error",
                text: "El alumno ya está registrado en un proyecto",
                icon: "warning"
            });
        }

        
    } catch (error) {
        console.log("Ha ocurrido un error en la transaccion " + error);
    }
  }

  
  function quitarTutor(idproyecto, idpersona){
    swal({
        title: "Retirar tutor",
        text: "¿Está seguro que desea retirar este tutor del proyecto?",
        icon: "warning",
        buttons: true,
        dangermode: true,
    }).then((asignacion)=> {
        if(asignacion){
           retirarTutor(idproyecto, idpersona);     
           
        } else {
        swal("La acción fue cancelada");    
        }
    });
  }

  async function retirarTutor(idproyecto, idpersona){
    try {
        const formData = new FormData();
        formData.append('idproyecto', idproyecto);
        formData.append('idpersona', idpersona);

        let resp = await fetch("controllers/tutores.php?op=__retirar", {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });
        json = await resp.json();
        if(json.mensaje=="OK"){
            swal({
                title: "Exito",
                text: "El tutor ha sido retirado del proyecto",
                icon: "success"
            }).then((ok)=>{
                if(ok){
                    location.reload();
                }
            });
        } if(json.mensaje=="ERROR"){
            swal({
                title: "Error",
                text: "Ha ocurrido un error al eliminar al tutor",
                icon: "warning"
            });
        }
    } catch (error) {
        console.log("Hay un error en el sistema" + error);
    }
  }


 
  async function getAlumnos() {
    var url = new URL(window.location.href);
    var params = new URLSearchParams(url.search);
    var id = params.get('id');
    try {
      let resp = await fetch("controllers/alumnos.php?op=__listado");
      json = await resp.json();
      if (json.status) {
        let data = json.data;
        let tableBody = document.querySelector("#tblAlumnos"); // Obtener el cuerpo de la tabla
        data.forEach(item => {
          let newtr = document.createElement("tr");
          newtr.id = "row_" + item.id;
          newtr.innerHTML = `
            <td class="d-xl-table-cell">${item.p_nombre} ${item.p_apellido}</td>
            <td class="d-xl-table-cell">${item.identificacion}</td>
            <td class="d-xl-table-cell">${item.celular}</td>
            <td class="d-xl-table-cell">${item.email}</td>
            <td class="d-md-table-cell">
            <button onclick="asignarAlumno(${item.idpersona}, ${id})" class="btn btn-warning"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-check icontb"></i></button> 
            </td>
          `;
          tableBody.appendChild(newtr); // Agregar el registro al cuerpo de la tabla
          
        });
      }
    } catch (error) {
      console.log("Ocurrio un error" + error);
    }
  }
  
  // Verificar si existe el elemento de tabla antes de llamar a getTutors()
  if (document.querySelector("#tblAlumnos")) {
    getAlumnos();
  }


   //buscador de tutores
   if(document.querySelector('#frmBuscaAlumnos')){
    let frmBuscar = document.querySelector('#frmBuscaAlumnos');
    let inputBusca = document.querySelector('#txtAlumnos');
    inputBusca.addEventListener("keyup", fnInpBuscaAl, true);


    frmBuscar.onsubmit = function(e){
        e.preventDefault();
        let busqueda = document.querySelector("#txtAlumnos").value;
        if(busqueda == ""){
            document.querySelector('#tblAlumnos').innerHTML=""
            getAlumnos();
        } else {
            fnBuscarAlumno();
        }

    }
    
}

async function fnBuscarAlumno(busqueda){
    document.querySelector('#tblAlumnos').innerHTML="";
    var url = new URL(window.location.href);
    var params = new URLSearchParams(url.search);
    var id = params.get('id');
    try {

        let formData = new FormData();
        formData.append('buscar', busqueda);
        let resp = await fetch('controllers/alumnos.php?op=__buscar', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });

        json = await resp.json();
        if (json.status){
            let data = json.data;
            data.forEach(item => {
                let newtr = document.createElement("tr");
                newtr.id = "row_"+item.id;
                newtr.innerHTML=`
                    <tr>
                        <td class="d-xl-table-cell">${item.p_nombre} ${item.p_apellido}</td>
                        <td class="d-xl-table-cell">${item.identificacion}</td>
                        <td class="d-xl-table-cell">${item.celular}</td>
                        <td class="d-xl-table-cell">${item.email}</td>
                        <td class="d-md-table-cell">
                        <button onclick="asignarAlumno(${item.idpersona}, ${id})" class="btn btn-warning"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-check icontb"></i></button> 
                        </td>
                    </tr>`;
                    document.querySelector("#tblAlumnos").innerHTML="";
                    document.querySelector("#tblAlumnos").appendChild(newtr);
                    
            });
        }

        
    } catch (error) {
        console.log("Error en la solicitud" +error)
        
    }

}

function fnInpBusca(){
    let busqueda = document.querySelector("#txtbusca").value;
        if(busqueda == ""){
            document.querySelector('#tblAlumnos').innerHTML=""
            getAlumnos();
        } else {
            fnBuscarAlumno(busqueda);
        }
  }


  function quitarAlumno(idproyecto, idpersona){
    swal({
        title: "Retirar Alumno",
        text: "¿Está seguro que desea retirar a este alumno del proyecto?",
        icon: "warning",
        buttons: true,
        dangermode: true,
    }).then((asignacion)=> {
        if(asignacion){
           retirarAlumno(idproyecto, idpersona);     
          
        } else {
        swal("La acción fue cancelada");    
        }
    });
  }

  async function retirarAlumno(idproyecto, idpersona){
    try {
        const formData = new FormData();
        formData.append('idproyecto', idproyecto);
        formData.append('idpersona', idpersona);

        let resp = await fetch("controllers/alumnos.php?op=__retirar", {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });
        json = await resp.json();
        if(json.mensaje=="OK"){
            swal({
                title: "Exito",
                text: "El alumno ha sido retirado del proyecto",
                icon: "success"
            }).then((ok)=>{
                if(ok){
                    location.reload();
                }
            });
        } if(json.mensaje=="MIN"){
            swal({
                title: "Error",
                text: "Debe haber un estudiante registrado en el proyecto",
                icon: "warning"
            });
        }
    } catch (error) {
        console.log("Hay un error en el sistema" + error);
    }
  }


  function borraralumno(id){
    swal({
        title: "¿Está seguro?",
        text: "¿Está seguro que desea eliminar el registro? Esta acción no se puede deshacer",
        icon: "warning",
        buttons: true,
        dangermode: true,
    }).then((willdelete)=> {
        if(willdelete){
        fncDeleteAlumno(id);    
          //location.reload();
        } else {
        swal("La acción fue cancelada");    
        }
    });
}


async function fncDeleteAlumno(id) {
    try {
      let formData = new FormData();
      formData.append('id', id); 
      let response = await fetch("controllers/alumnos.php?op=__delete",{
        method: 'post',
        mode: 'cors',
        body: formData,
        cache: 'no-cache',
      });
      console.log(response.msg);
      if (response.msg) {
        switch(response.msg){
          case 'SUCCESS':
            location.reload();
          break;
          case 'ERROR':
            console.log("Error eliminando")
          break;
        }
      }
    } catch (error) {
      console.log("Ocurrio un error:" + error);
    }
  }

  function fnInpBuscaAl(){
    let busqueda = document.querySelector("#txtAlumnos").value;
        if(busqueda == ""){
            document.querySelector('#tblAlumnos').innerHTML=""
            getAlumnos();
        } else {
            fnBuscarAlumno(busqueda)
        }
  }


  
  async function MostrarAlumnos(pgact, registros) {
    var pgact = pgact??1;
    var registros = registros ??5;

    let sd = new FormData();
    sd.append("pg", parseInt(pgact));
    sd.append("registros", parseInt(registros));
    try {
      let resp = await fetch("controllers/alumnos.php?op=__paginado",{
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        body: sd
      });
      json = await resp.json();
      if (json.status) {
        let data = json.data;
        document.querySelector('#tbltodo').innerHTML=""
        let paginas = json.total_paginas;
        let tableBody = document.querySelector("#tbltodo"); // Obtener el cuerpo de la tabla
        data.forEach(item => {
          let newtr = document.createElement("tr");
          newtr.id = "row_" + item.id;
          newtr.innerHTML = `
            <td class="d-xl-table-cell">${item.p_nombre} ${item.p_apellido}</td>
            <td class="d-xl-table-cell">${item.identificacion}</td>
            <td class="d-xl-table-cell">${item.celular}</td>
            <td class="d-xl-table-cell">${item.email}</td>
            <td class="d-md-table-cell">
            <button onclick="editAlumno(${item.idpersona})" class="btn btn-warning"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-edit icontb"></i></button> 
            <button onclick="borraralumno(${item.idpersona})" class="btn btn-danger"  data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar a proyecto"><i class="fa-solid fa-trash-can icontb"></i></button> 
            </td>
          `;
          tableBody.appendChild(newtr); // Agregar el registro al cuerpo de la tabla
            const paginacion = document.getElementById('paginas');
            if (pgact == 1) {
                paginacion.innerHTML = `<li><a href="#">«</a></li>`;
            } else {
                let pba = pgact - 1;
                paginacion.innerHTML = `<li><a href="#" onclick="MostrarAlumnos(${pba}, ${registros})">«</a></li>`;
            }
            for (let i = 1; i <= paginas; i++) {
                if (pgact == i) {
                    paginacion.innerHTML += `<li><a class="active" href="#" onclick="MostrarAlumnos(${i}, ${registros})">${i}</a></li>`;
                } else {
                    paginacion.innerHTML += `<li><a href="#" onclick="MostrarAlumnos(${i}, ${registros})">${i}</a></li>`;
                }
            }
            
            if (pgact == paginas) {
                paginacion.innerHTML += `<li><a href="#">»</a></li>`;
            } else {
                let pgn = pgact + 1;
                paginacion.innerHTML += `<li><a href="#" onclick="MostrarAlumnos(${pgn}, ${registros})">»</a></li>`;
            }

        });
      }
    } catch (error) {
      console.log("Ocurrio un error" + error);
    }
  }
  
  // Verificar si existe el elemento de tabla antes de llamar a getTutors()
  if (document.querySelector("#tbltodo")) {
    const registros = document.getElementById('registros').value;
    MostrarAlumnos(1, registros);
  }

  
  
  if(document.getElementById('registros')){
    const slpg = document.getElementById('registros');
    slpg.addEventListener('change', function(){
        var registrosPagina = this.value;
        MostrarAlumnos(1, registrosPagina);
      });
  }
  
  