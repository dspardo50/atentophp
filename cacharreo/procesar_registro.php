<?php
require_once '../ejercicio29.php'; // Asume que tienes un archivo para conectar a la base de datos.

$email = $_POST['email'] ?? '';
$password = $_POST['password'] ?? '';
$password_confirm = $_POST['password_confirm'] ?? '';

// Validar que las contraseñas coincidan
if ($password !== $password_confirm) {
    die('Las contraseñas no coinciden.');
}

// Verificar si el correo ya está registrado
$sql = "SELECT * FROM usuarios WHERE email = ?";
$stmt = $conexion->prepare($sql);
$stmt->bind_param('s', $email);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    die('Este correo electrónico ya está registrado.');
}

// Cifrar la contraseña
$hashed_password = password_hash($password, PASSWORD_DEFAULT);

// Insertar el nuevo usuario en la base de datos
$sql = "INSERT INTO usuarios (email, password) VALUES (?, ?)";
$stmt = $conexion->prepare($sql);
$stmt->bind_param('ss', $email, $hashed_password);

if ($stmt->execute()) {
    echo 'Registro exitoso.';
} else {
    echo 'Error en el registro: ' . $stmt->error;
}

$stmt->close();
?>
