<?php
require_once "../ejercicio29.php";

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Primero, obten el nombre del archivo del registro
    $consulta = $conexion->prepare("SELECT archivo FROM documento WHERE id = ?");
    $consulta->bind_param("i", $id);
    $consulta->execute();
    $resultado = $consulta->get_result();
    $fila = $resultado->fetch_assoc();
    $nombreArchivo = $fila['archivo'];
//$documento = $resultado->fetch_assoc(); en Modificar
    // Ahora, puedes eliminar el archivo
    $rutaCompleta = __DIR__. '/pedeefes/' . $nombreArchivo;
    if (file_exists($rutaCompleta)) {
        unlink($rutaCompleta);
        echo "El archivo ha sido eliminado.";
        echo "<script>window.close();</script>";

    } else {
        echo "El archivo no existe.";
    }

    // Luego, puedes eliminar el registro de la base de datos
    
}
//header('Location: tabla.php');
?>