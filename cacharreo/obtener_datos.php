<?php
// Obtén los datos actualizados de la base de datos
require_once "../ejercicio29.php";

$consulta = mysqli_query($conexion, "SELECT * FROM usuarioa");
$num_results = mysqli_num_rows($consulta);

// Genera una tabla HTML con los datos
echo '<thead><tr><th>id</th><th>Primer nombre</th><th>Segundo nombre</th><th>Primer apellido</th><th>Segundo apellido</th><th>documentos</th><th>acciones</th></tr></thead><tbody>';

while ($fila = mysqli_fetch_assoc($consulta)) {
    echo '<tr>';
    // Llena la tabla con los datos
    echo '<td>' . $fila['id'] . '</td>';
    echo '<td>' . $fila['primer_nombre'] . '</td>';
    echo '<td>' . $fila['segundo_nombre'] . '</td>';
    echo '<td>' . $fila['primer_apellido'] . '</td>';
    echo '<td>' . $fila['segundo_apellido'] . '</td>';
    echo '<td>' . $fila['documentos'] . '</td>';
    echo '<td><a href="eliminar.php?id=' . $fila['id'] . '" class="btn btn-primary delete-button"><i class="fa fa-download"></i> Eliminar</a>';
    echo '<a href="Modificar.php?id=' . $fila['id'] . '" class="btn btn-primary modifi-button"><i class="fa fa-download"></i> Actualizar</a></td>';
    echo '</tr>';
}

echo '</tbody>';
?>
