-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:4000
-- Tiempo de generación: 01-11-2023 a las 20:24:10
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `atento`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioa`
--

CREATE TABLE `usuarioa` (
  `id` int(11) NOT NULL,
  `primer_nombre` varchar(500) NOT NULL,
  `segundo_nombre` varchar(500) NOT NULL,
  `primer_apellido` varchar(500) NOT NULL,
  `segundo_apellido` varchar(500) NOT NULL,
  `documentos` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarioa`
--

INSERT INTO `usuarioa` (`id`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `documentos`) VALUES
(2, 'Ricardo Arjon', 'Pardo Cilla', 'Villalba rar', 'Fabian lol', 'tambien estan xd'),
(3, 'Juan', 'Diego', 'Pedraza', 'Campos', 'Si'),
(4, 'Ramire', 'panchita', 'Colorado', 'Martinez Presiado', 'azul'),
(5, 'Santiago', 'Pardo', 'Villalba', 'Villalobos', 'tal vez'),
(6, 'Pistacho', 'Angela', 'Del', 'Rosario', 'go'),
(7, 'sos', 'asa', 'afdfa', 'fasa', 'fas'),
(8, 'tabla', 'actualizada', 'aver', 'rojo', 'veamos'),
(10, 'revisoa', 'tecono', 'mecanica', 'opa', 'sasd'),
(12, 'Ricardo', 'dasf', 'asdg', 'agsd', 'gas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarioa`
--
ALTER TABLE `usuarioa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarioa`
--
ALTER TABLE `usuarioa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
