<?php
include 'header.php';
require_once "../ejercicio29.php";

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    // Luego, puedes eliminar el registro de la base de datos
    $eliminar_en_sql = $conexion->prepare("DELETE FROM `usuarioa` WHERE `id` = ?");
    $eliminar_en_sql->bind_param("i", $id);
    $eliminar_en_sql->execute();

    if($eliminar_en_sql->affected_rows >= 1){
        echo "Registro eliminado con éxito.";
    }else{
        echo "Hubo un error al intentar eliminar el registro.";
    }
}
//header('Location: tabla.php');

?>
<button onclick="location.href='tabla.php'" class="btn btn-warning">Volver</button>