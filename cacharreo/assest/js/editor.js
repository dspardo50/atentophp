if ($('#contenido')){
$('#contenido').richText({

    // text formatting
    bold: true,
    italic: true,
    underline: true,
  
    // text alignment
    leftAlign: true,
    centerAlign: true,
    rightAlign: true,
    justify: true,
  
    // lists
    ol: true,
    ul: true,
  
    // title
    heading: true,
    fontSize: true,
  
    // uploads
    imageUpload: true,
    fileUpload: false,
  
    // code
    removeStyles: true,
    code: true,
  
    // dropdowns
    fileHTML: '',
    imageHTML: '',
  
    // preview
    preview: false,
  
    // placeholder
    placeholder: '',
  
    // dev settings
    useSingleQuotes: false,
    height: 0,
    heightPercentage: 0,
    id: "",
    class: "",
    useParagraph: false,
    maxlength: 0,
    useTabForNext: false,
  
    // callback function after init
    callback: undefined,
  
  });
}

if ($('#termsbox')){
  $('#termsbox').richText({

    // text formatting
    bold: true,
    italic: true,
    underline: true,
  
    // text alignment
    leftAlign: true,
    centerAlign: true,
    rightAlign: true,
    justify: true,
  
    // lists
    ol: true,
    ul: true,
  
    // title
    heading: true,
    fontSize: true,
  
    // uploads
    imageUpload: true,
    fileUpload: false,
  
    // code
    removeStyles: true,
    code: true,
  
    // dropdowns
    fileHTML: '',
    imageHTML: '',
  
    // preview
    preview: false,
  
    // placeholder
    placeholder: '',
  
    // dev settings
    useSingleQuotes: false,
    height: 0,
    heightPercentage: 0,
    id: "",
    class: "",
    useParagraph: false,
    maxlength: 0,
    useTabForNext: false,
  
    // callback function after init
    callback: undefined,
  
  });
}


  if ($('#txtprivacy')){
  $('#txtprivacy').richText({

    // text formatting
    bold: true,
    italic: true,
    underline: true,
  
    // text alignment
    leftAlign: true,
    centerAlign: true,
    rightAlign: true,
    justify: true,
  
    // lists
    ol: true,
    ul: true,
  
    // title
    heading: true,
    fontSize: true,
  
    // uploads
    imageUpload: true,
    fileUpload: false,
  
    // code
    removeStyles: true,
    code: true,
  
    // dropdowns
    fileHTML: '',
    imageHTML: '',
  
    // preview
    preview: false,
  
    // placeholder
    placeholder: '',
  
    // dev settings
    useSingleQuotes: false,
    height: 0,
    heightPercentage: 0,
    id: "",
    class: "",
    useParagraph: false,
    maxlength: 0,
    useTabForNext: false,
  
    // callback function after init
    callback: undefined,
  
  });
}


if ($('#boxtestimonio')){
  $('#boxtestimonio').richText({

    // text formatting
    bold: true,
    italic: true,
    underline: true,
  
    // text alignment
    leftAlign: true,
    centerAlign: true,
    rightAlign: true,
    justify: true,
  
    // lists
    ol: true,
    ul: true,
  
    // title
    heading: true,
    fontSize: true,
  
    // uploads
    imageUpload: true,
    fileUpload: false,
  
    // code
    removeStyles: true,
    code: true,
  
    // dropdowns
    fileHTML: '',
    imageHTML: '',
  
    // preview
    preview: false,
  
    // placeholder
    placeholder: '',
  
    // dev settings
    useSingleQuotes: false,
    height: 0,
    heightPercentage: 0,
    id: "",
    class: "",
    useParagraph: false,
    maxlength: 0,
    useTabForNext: false,
  
    // callback function after init
    callback: undefined,
  
  });
}

if ($('#boxrespuesta')){
  $('#boxrespuesta').richText({

    // text formatting
    bold: true,
    italic: true,
    underline: true,
  
    // text alignment
    leftAlign: true,
    centerAlign: true,
    rightAlign: true,
    justify: true,
  
    // lists
    ol: true,
    ul: true,
  
    // title
    heading: true,
    fontSize: true,
  
    // uploads
    imageUpload: true,
    fileUpload: false,
  
    // code
    removeStyles: true,
    code: true,
  
    // dropdowns
    fileHTML: '',
    imageHTML: '',
  
    // preview
    preview: false,
  
    // placeholder
    placeholder: '',
  
    // dev settings
    useSingleQuotes: false,
    height: 0,
    heightPercentage: 0,
    id: "",
    class: "",
    useParagraph: false,
    maxlength: 0,
    useTabForNext: false,
  
    // callback function after init
    callback: undefined,
  
  });
}