function actualizarTabla() {
    var tabla = document.getElementById("tabla"); // Identifica la tabla que deseas actualizar
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "obtener_datos.php", true); // Reemplaza "obtener_datos.php" con la URL correcta para obtener los datos actualizados
  
    xhr.onload = function () {
      if (xhr.status === 200) {
        // Actualiza la tabla con los nuevos datos
        tabla.innerHTML = xhr.responseText;
      }
    };
  
    xhr.send();
  }
  
  setInterval(actualizarTabla, 4000); // Realiza la actualización cada 4 segundos
  